# Dropbox
https://www.dropbox.com/sh/3ur33hkiaeq6ur2/AACSSxRtwQaHGcHN6rHgRGqza?dl=0

Chào mọi người, buổi học sau chúng ta sẽ học về 2 cấu trúc cơ bản là Stack và Queue. Cũng như Sort, chúng ta không học lại cách cài đặt, mà mục tiêu là nắm rõ nguyên lý và cách ứng dụng nó vào bài toán. 
Mọi người xem trước lý thuyết ở link bên dưới nhé!
# Stacks and Queues
https://www.cs.cmu.edu/~adamchik/15-121/lectures/Stacks%20and%20Queues/Stacks%20and%20Queues.html

# Ex1
https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1876

# Ex2
https://www.spoj.com/problems/STPAR/
