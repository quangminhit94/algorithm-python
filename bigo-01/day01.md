
#BTVN_Buoi01 Tài liệu học tập Buổi 1. Hướng dẫn sử dụng:

1. Thư mục "Solution_BTTL_Buoi01_DynamicArrays & String" Bài giải các bài tập trên lớp (được hướng dẫn chi tiết ý tưởng, cách làm, có source code minh họa bằng C++, Python, Java). Lưu ý: các bạn nên hạn chế xem solution, khi nào thật sự đã suy nghĩ rất nhiều nhưng vẫn chưa làm được thì hãy xem nhé.

2. Tập tin "BTVN_Buoi01_DynamicArrays & String.txt". Chứa các bài tập về nhà (3 bài), các BTVN các bạn làm các bạn sẽ nộp lại cho Big-O vào link dropbox bên trong tập tin. Khi bạn nộp bài bạn sẽ được trợ giảng review lại và chỉnh các lỗi về tư duy và coding style. Các bạn nộp bài bằng cách bỏ vào 1 thư mục chứa:
- 3 tập tin source code (*.cpp, *.java, *.py) mỗi bài 1 tập tin không nộp cả project. Trên đầu mỗi tập tin nên có ghi ý tưởng về cách làm để việc tiện cho trợ giảng review bài các bạn.

=> Sau đó đặt tên folder chứa tất cả tập tin trên bằng tên Email của bạn, rồi nén lại: ví dụ bigocoding.rar (bỏ đuôi @gmail.com, việc đặt tên như thế này để các bạn trợ giảng sau khi review xong sẽ gửi về Email cá nhân của bạn, nhận xét về bài làm các bạn)

- Sau khi nén xong bạn sẽ nộp qua link dropbox đã có sẵn trong tập tin BTVN.
*Lưu ý: Các bạn vui lòng làm giống như trên để tiện cho việc review các bạn nhé.

- Bài tập thêm: dành cho những bạn nào làm hết BTVN muốn làm thêm thì làm thêm các bài tập này, các bài này sẽ không có sửa tại lớp (vì không đủ thời gian), nhưng sẽ có gửi tập tin hướng dẫn chi tiết giải cho các bạn.

3. Tập tin "Lecture02_AlgorithmicComplexity.txt" Bài đọc trước buổi 2 để các bạn xem trước kịp tiếp thu bài học tốt hơn (nếu các bạn không có thời gian đọc trước thì cũng đừng quá lo lắng nhé, bài giảng sẽ được giảng lại chi tiết tại buổi học).

Chúc các bạn làm bài tốt.

# Slide
https://www.slideshare.net/secret/MDvRKIVawYC4FS

# Dropbox
https://www.dropbox.com/sh/j1orwvzfaxpn02o/AAAnD__UZkxjn3QhKtGzG20ja?dl=0

# Ex1
http://codeforces.com/problemset/problem/691/A

# Ex2
http://codeforces.com/problemset/problem/731/A
