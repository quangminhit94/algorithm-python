Chào mọi người, buổi học tới chúng ta sẽ học về Sorting. Đây có lẽ là một nội dung không mới, tuy nhiên ta không thể phủ nhận tầm quan trọng của nó.

Trong buổi học này chúng ta sẽ không học lại cách cài đặt mà tập trung vào ứng dụng của thuật toán sắp xếp, học cách tư duy bài toán, biết được khi nào sử dụng và sử dụng nó như thế nào cho hiệu quả.

Để chuẩn bị tốt cho buổi học, mời mọi người xem trước lý thuyết ở đường link bên dưới nhé!

C++: http://www.fredosaurus.com/notes-cpp/algorithms/sorting/stl-sort-arrays.html
Python: https://wiki.python.org/moin/HowTo/Sorting
Java: http://robertovormittag.net/array-sorting-algorithms-in-the-java-library/

# Dropbox:
https://www.dropbox.com/sh/mk0o2y18cfo373z/AABnwOfeArIry3hMlbRSgbvva?dl=0

# Ex1
http://codeforces.com/problemset/problem/451/B

# Ex2
http://codeforces.com/problemset/problem/169/A