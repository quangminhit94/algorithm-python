# Slide
https://www.slideshare.net/secret/2WSDXasNqpbk3C

# Dropbox:
https://www.dropbox.com/sh/e8541s894h6zkxb/AADfEb1-uJX0AeUCuv1WHoLXa?dl=0


Chào mọi người, buổi tới đây ta sẽ được học về một phần kiến thức rất quan trọng là Độ phức tạp của Thuật toán (Algorithmic Complexity). Phân tích độ phức tạp Thuật toán giúp ta trả lời câu hỏi: Liệu Thuật toán của mình có chạy trong thời gian cho phép, làm sao để code mình chạy nhanh hơn, hay tốn ít bộ nhớ hơn, song vẫn giữ được tính đúng đắn của nó?

Chính vì thế mọi người nhớ đi học đầy đủ nhé. Mọi người có thể tham khảo nội dung buổi học tiếp theo qua link sau:
# Algorithmic Complexity
https://www.cs.cmu.edu/~adamchik/15-121/lectures/Algorithmic%20Complexity/complexity.html

# Ex1
http://codeforces.com/problemset/problem/224/B
